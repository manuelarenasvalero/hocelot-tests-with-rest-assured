package restassured;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
 
public class userTests {
 
    @Before
    public void before(){
        RestAssured.baseURI = "http://datajuridica.com";
    }
  
    @Test
    public void userExists(){
        RestAssured
            .given().log().all()
                .contentType("application/json")
                .body("{\"Nombre\" : \"ruth navaja\"}")
                .post("/Result")
            .then().log().all()
                .statusCode(200)
                .body(containsString("RUTH NAVAJA CASTILLO"))
                .and()
                .body(containsString("*****040"))
            ;
    }
      
    @Test
    public void userNotExists(){ 
        given().log().all()
        .contentType("application/json")
            .body("{\"Nombre\" : \"johann bach\"}")
            .post("/Result")
        .then().log().ifValidationFails()
            .statusCode(200)
            .and()
            .body(containsString("NO SE ENCUENTRA INFORMACION PARA JOHANN BACH"))
        ;
    }
 
}